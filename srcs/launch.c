/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   launch.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/13 12:26:03 by dmurovts          #+#    #+#             */
/*   Updated: 2017/10/14 12:03:17 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	ft_pxl_img(int x, int y, int rgb, t_mlx *mlx)
{
	int				bpp;
	int				sl;
	int				en;
	char			*image;
	unsigned int	tmp;

	image = mlx_get_data_addr(mlx->img, &bpp, &sl, &en);
	tmp = (mlx_get_color_value(mlx->init, rgb));
	if (x > 0 && x < WIN_W && y > 0 && y < WIN_H)
	{
		ft_memcpy((void *)(image + sl * y + x * sl / WIN_W),
			(void *)&tmp, 4);
	}
}

void	ft_mlx_init(t_mlx *mlx)
{
	mlx->init = mlx_init();
	mlx->win = mlx_new_window(mlx->init, WIN_W, WIN_H, "RTv1");
	mlx->img = mlx_new_image(mlx->init, WIN_W, WIN_H);
}

void	ft_risova4(t_deusx *deus)
{
	int x;
	int y;
	int z;
}

void	ft_laucnh(t_deusx *deus)
{
	deus->mlx = (t_mlx *)malloc(sizeof(t_mlx));
	ft_mlx_init(deus->mlx);

	mlx_put_image_to_window(mlx->init, mlx->win, mlx->img, 0, 0);
}

