/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/30 10:57:50 by dmurovts          #+#    #+#             */
/*   Updated: 2017/10/13 12:08:14 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int     ft_cnt_lines(int fd)
{
    int i;
    int gnl;
    char *tmp;

    i = 0;
    while ((gnl = get_next_line(fd, &tmp)))
    {
        if (gnl > 0)
        {
            free(tmp);
            i++;
        }
        else
            return(-1);
    }
    close(fd);
    return (i);
}

char    **ft_read(char *input)
{
    int     fd;
    int     depth;
    int     i;
    char    **result;
    char    *tmp;

    if ((fd = open(input, O_RDONLY)) < 0)
    {
        ft_error();
        return (NULL);
    }
    if ((depth = ft_cnt_lines(fd)) > 0)
        result = (char **)malloc(sizeof(char *) * (depth + 1));
    else
        return (0);
    i = 0;
    if ((fd = open(input, O_RDONLY)) < 0)
    {
        ft_error();
        return (NULL);
    }
    while (get_next_line(fd, &tmp) > 0)
    {
        if (tmp[0] != '#' && tmp[0])
        {
            result[i] = ft_strdup(tmp);
            i++;
        }
        free(tmp);
    }
    result[i] = NULL;
    return (result);
}

int     ft_arrlen(char **arr)
{
    int i;

    i = -1;
    while (arr[++i])
        ;
    return (i);
}

int        ft_shaker(char *inp)
{
    if (ft_strcmp(ft_strtrim(inp), "%Light") == 0)
        return (1);
    else if ((ft_strcmp(ft_strtrim(inp), "%Camera") == 0))
        return (2);
    else
        return (3);
}


void       ft_distribute_values(t_shape *dest, char **src)
{
    dest->x = ft_atoi(src[0]);
    dest->y = ft_atoi(src[1]);
    dest->z = ft_atoi(src[2]);
    dest->nx = ft_atoi(src[3]);
    dest->ny = ft_atoi(src[4]);
    dest->nz = ft_atoi(src[5]);
    dest->r = ft_atoi(src[6]);
    dest->a = ft_atoi(src[7]);
    dest->h = ft_atoi(src[8]);
    printf("dest->h = %i\n", dest->h);
}

t_shape    **ft_populate(char **map, t_deusx *deus)
{
    int     i;
    int     j;
    int     arrlen;
    char    **tmp;
    char    **tmp2;
    t_shape **scene;

    i = -1;
    j = 0;
    arrlen = ft_arrlen(map);
    scene = (t_shape **)malloc(sizeof(t_shape *) * (arrlen - 2));
    while (++i < arrlen)
    {
     tmp = ft_strsplit(map[i], '|');
     tmp2 = ft_strsplit(tmp[1], ':');
     if (ft_shaker(tmp[0]) == 1)
          ft_distribute_values(deus->light,tmp2);
     else if (ft_shaker(tmp[0]) == 2)
       ft_distribute_values(deus->cam, tmp2);
     else
     {
         scene[j] = (t_shape *)malloc(sizeof(t_shape));
         ft_distribute_values(scene[j], tmp2);
         j++;
     }
     free(tmp);
     free(tmp2);
    }
    return (scene);
}
