/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 15:34:19 by dmurovts          #+#    #+#             */
/*   Updated: 2017/10/13 16:02:25 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int    ft_error(void)
{
    ft_putendl("error");
    return (0);
}

int main(int argc, char **argv)
{
    char    **map;
    t_deusx  *deus;

    if (argc) 
        ;
    map = ft_read(argv[1]);
    deus = (t_deusx *)malloc(sizeof(t_deusx));
    deus->light = (t_shape *)malloc(sizeof(t_shape));
    deus->cam = (t_shape *)malloc(sizeof(t_shape));
    deus->scene = ft_populate(map, deus);
    ft_launch(deus);
    return (0);
}
