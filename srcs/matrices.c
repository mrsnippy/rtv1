/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrices.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/14 12:03:44 by dmurovts          #+#    #+#             */
/*   Updated: 2017/10/14 12:58:54 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int		**ft_view_matrix()
{
	int		**view;
	int		i;

	i = -1;
	view = (int **)malloc(sizeof(int *) * 4);
	while (++i < 4)
		view[i] = (int *)malloc(sizeof(int) * 4);
	

}