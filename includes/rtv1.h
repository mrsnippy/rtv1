/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rtv1.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 15:29:31 by dmurovts          #+#    #+#             */
/*   Updated: 2017/10/14 14:44:48 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV1_H
# define RTV1_H

# define WIN_W 1200
# define WIN_H 1200

# include "libft.h"
# include <mlx.h>
# include <math.h>
# include <fcntl.h>
# include <time.h>
#include <stdio.h>

typedef struct s_shape {
    int x;
    int y;
    int z;
    int nx;
    int ny;
    int nz;
    int r;
    int a;
    int h;
}               t_shape;

typedef struct  s_mlx {
    void    *init;
    void    *win;
    void    *img;
}               t_mlx;


typedef struct s_deusx {
    t_shape *light;
    t_shape *cam;
    t_shape **scene;
    t_mlx   *mlx;
}               t_deusx;

t_shape     **ft_populate(char **map, t_deusx *deus);
char        **ft_read(char *input);
int         ft_error(void);
void        ft_launch(t_deusx *deus);

#endif
